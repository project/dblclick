Double Click Module
by Jeff Robbins
www.lullabot.com

This module allows you go to the edit page by double clicking on any 
internal link to an item.

Be sure to enable 'double click' permission to include the Double Click 
Javascript "magic" for users in a given role.

Additionally, users with 'administer nodes' permission can double click 
on any link to a node to jump to its editing page. Users with 
'administer users' permission can double click on any link to a user 
page to jump to the editing page for that user.

This module also creates hook_dblclick() which allows modules to provide 
double-click behaviors. See the module code for documentation and examples.

This module requires the JQuery Javascript library and therefore 
requires Drupal 5.