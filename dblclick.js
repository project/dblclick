
Drupal.dblClick = new Object();

Drupal.dblClick.clicker = function(e) {
  var self = this;
  if (!e.metaKey && !e.ctrlKey) { // don't do this stuff if meta or control key is pressed
	  var doClick = function() {
	    // allow other scripts to do their stuff
	    $return = typeof(self.oldclick) == 'function' ? self.oldclick() : true;
	    if ($return != false) { // if other scripts don't return false
	      location.href = self.href; // bug: safari doesn't stick this in the history
	    }
	  }
	  if (Drupal.dblClick.clickTimer) {
	    window.clearTimeout(Drupal.dblClick.clickTimer);
	  }
	  Drupal.dblClick.clickTimer = window.setTimeout(doClick, Drupal.settings.dblClick.dblTime);
	  return false;
  }
}

Drupal.dblClick.dblClicker = function(e) {
  var self = this;
  window.clearTimeout(Drupal.dblClick.clickTimer);
  var arg = (self.href.indexOf('?') == -1) ? '?dbl=true' : '&dbl=true';
  var parts = self.href.split('#');
  var path = parts.shift();
  var frag = parts.shift();
  if (frag) {
    arg = arg + '&frag=' + frag;
    frag = '#' + frag;
  }
  else {
    frag = '';
  }
  arg += e.shiftKey ? '&shift=true' : '';
  arg += e.altKey ? '&alt=true' : '';
  arg += Drupal.settings.dblClick.dblDest ? Drupal.settings.dblClick.dblDest : '';
  location.href = path + arg + frag;
  return false;
}

if (Drupal.jsEnabled) {
  // jQuery sweetness by the real John Resig.
  jQuery.expr[":"].exclude = "$(m[3]).index(a)<0";
  
  $(document).ready(function() {
    // find all of the internal (relative) links
    // move their old click behaviors into 'oldclick' so we can execute it later
    // exclude blocks from dhtml_menu module because of conflicts
    $('a[@href]:not("[@href*=":"]):not([@onclick]):exclude(div[@id^="block-dhtml_menu"] a)')
      .each(function(){this.oldclick = this.onclick})
      .unclick()
      .click(Drupal.dblClick.clicker)
      .dblclick(Drupal.dblClick.dblClicker);
  });
}